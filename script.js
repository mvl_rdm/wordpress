jQuery(document).ready(function(){

    // Loadmore function
    jQuery.fn.mvl_loadmore = function(val) {

        var loadmore_text = this[0].innerText;
        $(this).click(function(e){
            e.preventDefault();

            var button = $(this),
                data = {
            'action': val.action,
            'query': val.query,
            'page' : val.current_page,
            'get_template_part' : val.get_template_part
            };
        
            jQuery.ajax({
            url : params.ajaxurl,
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                button.text(val.loading_text);

            },
            success : function( data ){
                if( data ) { 
                button.text( loadmore_text ).parent().before(data);
                val.current_page++;
                if ( val.current_page == val.max_page ){
                    button.remove();       
                }
                } else {
                button.remove();
                }

                val.onSuccess();
            }
            });
        });
        
       return;
    }; 

});
