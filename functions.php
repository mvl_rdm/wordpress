<?php
function mvl_loadmore(){ 
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1;
 
	query_posts($args);
 
	if(have_posts()) : 
		while( have_posts() ): the_post();
          	get_template_part($_POST['get_template_part']);
		endwhile; 
	endif;
	die;
}
  
add_action('wp_ajax_{action_name_here}', 'mvl_loadmore');
add_action('wp_ajax_nopriv_{action_name_here}', 'mvl_loadmore');