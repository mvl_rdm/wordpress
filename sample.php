<?php
    $_Blog = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => 3
    ));
    ?>
    <?php if( $_Blog->have_posts() ) : ?>
        <?php while( $_Blog->have_posts() ) : $_Blog->the_post(); ?>
            
            <?php get_template_part('template/spot-blog'); ?>

        <?php endwhile; ?>
            
        <?php wp_reset_query() ?>
        <?php if (  $_Blog->max_num_pages > 1 ) : ?>
            <div class="posts_loadmore_div">
                <a href="#" id="posts_loadmore" class="posts_loadmore btn btn-tertiary btn-filled">CARREGAR MAIS</a>
            </div>
            <script>
            $(document).ready(function(){
                current_page = <?php echo $_Blog->query_vars['paged']; ?>;
                $('#posts_loadmore').mvl_loadmore({
                    action: "action_name_here", // Create wp_action('wp_ajax_{action_name_here}' , ...) and wp_action('wp_ajax_nopriv_{action_name_here}' , ...) on functions.php
                    loading_text : "Carregando...", // Loading text
                    get_template_part : "template/spot-blog", // Template on while
                    query : '<?php echo json_encode( $_Blog->query ) ?>', // Query
                    current_page : (current_page) ? current_page : 1, // Current Page
                    max_page : <?php echo $_Blog->max_num_pages ?>, // Max page
                    onSuccess: function(){
                        // On Success event
                    }
                });
            });
            </script>
        <?php endif; ?>
    <?php else : ?>

    <?php endif; ?>
