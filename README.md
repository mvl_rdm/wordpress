# Função Loadmore para Wordpress #

Função que facilita a inclusão de botão de _loadmore_ para posts customizados no Wordpress

1. Insira o [script](https://bitbucket.org/mvl_rdm/wordpress/src/master/script.js) no seu arquivo de scripts do Wordpress
2. Faça a chamada da função $(element).mvl_loadmore({...}), conforme o arquivo [sample.php](https://bitbucket.org/mvl_rdm/wordpress/src/master/sample.php)
3. Cria as actions no arquivo functions.php de acordo com o arquivo [functions.php](https://bitbucket.org/mvl_rdm/wordpress/src/master/functions.php)